const express = require("express");

// Mongoose is a package thath allows creating of schemas to our model data structure
// Laso has access to a number of method for manipulating our database 
const mongoose = require("mongoose")


const app = express();
const port = 3001;


// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://derickjayyy12345:admin123@zuitt-bootcamp.mx8pwpr.mongodb.net/s35?retryWrites=true&w=majority",
	{
		// allows us to avaoid any current and future errors while connecting 
		useNewUrlparser: true,
		useUnifiedTopology: true
	}	

);

let db = mongoose.connection;

// concole.error.bind(consoole) allows us to ptrint errors in the browser console and in the terminal 
// "connection error" is the message that will display if an error is encountered 
db.on("error", console.error.bind(console, "connection error"));
// if the connection is successful, out in the console
db.once("open", () => console.log("We're connected to the cloud database"));



//Connecting to MongoDB Atlas END

app.use(express.json());
app.use(express.urlencoded({extended: true}));


// [SECTION] Mongoose Schemas 

// const taskSchema = new mongoose.Schema({
// 	name: String,
// 	email: String,
// 	password: String,
// 	status : {
// 		type:  String,
// 	default : "pending"
// 	}
// })

// [SECTION] Mongoose Models
/*const Task = mongoose.model("Task", taskSchema)

app.listen(port, () => console.log(`Server running at port ${port}`));


*/

//Creating of Task Application
// Create a new task
/*
BUSINESS LOGIC
Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database
The task data will be coming from the request's body
Create a new Task object with a "name" field/property
The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/



/*app.post("/tasks",(req, res) => {
Task.findOne({name: req.body.name}).then((result,err) => {
	if(result != null && result.name == req.body.name) {
		return res.send("Duplicate task found");
	} else {
		let newTask = new Task({
			name: req.body.name
		});
		newTask.save().then((savedTask,saveErr)   => {
			if(saveErr) {
				return console.error(saveErr);
			} else {
				return res.status(201).send("New task created!");
			}
		})
	}
})
});

*/

//Getting all the tasks

/*
BUSINESS LOGIC
Retrieve all the documents
BUSINESS LOGIC
If an error is encountered, print the error
If no errors are found, send a success status back to the client/Postman and return an array of documents */

/*app.get("/tasks", (req,res) => {
	Task.find({}).then((result,err ) => {
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})*/




/////// ACTIVITY ////////////


const userSchema = new mongoose.Schema({
	name: String,
	email: String,
	password: String,
	status : {
		type:  String,
	default : "pending"
	}
})


const User = mongoose.model("User", userSchema)

app.listen(port, () => console.log(`Server running at port ${port}`));



app.post("/signup",(req, res) => {
User.findOne({name: req.body.name},{name: req.body.password}).then((result,err) => {
	if(result != null && result.name != req.body.name) {
		return res.send("Duplicate user found");
	} else {
		let newUser = new User({
			name: req.body.name ,
			password: req.body.password
		});
		newUser.save().then((savedUser,saveErr)   => {
			if(saveErr) {
				return console.error(saveErr);
			} else {
				return res.status(201).send("New User created!");
			}
		})
	}
})
});

